#!/usr/bin/env node
const execa = require('execa')

const sshConfig = require('ssh-config')
const chalk = require('chalk')
const cmd = require('../lib/command-parser')
const options = require('../lib/yargs-setup')
const homedir = require('os').homedir()

const fs = require('fs')
const path = require('path')

const add = require('../lib/commands/add')
const run = require('../lib/commands/run')
const ls = require('../lib/commands/ls')
const rm = require('../lib/commands/rm')

const createChoices = require('../lib/create-choices')

const execaOptions = {
  stderr: 'inherit'
}

const main = async () => {
  const command = options._.shift()
  const configPath = path.join(homedir, '.ssh/config')

  try {
    if (!fs.existsSync(configPath)) {
      fs.writeFileSync(configPath, '', 'utf8')
      return console.error(
        `${configPath} did not exist so it was created, however it has no entries, so you will need to add one.`
      )
    }
    const configFile = fs.readFileSync(configPath, 'utf8') + '\n'

    const hosts = sshConfig
      .parse(configFile)
      .filter(line => line.type === 1)
      .filter(item => item.param.toLowerCase() === 'host')
    const choices = createChoices(hosts)

    if (!command) {
      // User ran sshm with no params, show interactive

      await run(choices)
    } else {
      switch (command) {
        case 'add':
          const newEntry = await add(options)
          hosts.push(newEntry[0])
          fs.writeFileSync(configPath, hosts.toString().replace(/\n\n/g, '\n').toString())
          console.info('Configuration added to ssh config.')
          break
        case 'ls':
          ls(hosts)
          break
        case 'rm':
          const hostToRemove = options._.shift()
          const reducedHosts = rm(hosts, hostToRemove)
          if (reducedHosts) {
            fs.writeFileSync(
              configPath,
              reducedHosts.toString().replace(/\n\n/g, '\n')
            )
            console.info('Configuration removed from ssh config.')
          }
          break
        default:
          const host = command

          return execa(
            ...cmd(
              `ssh ${host}`,
              Object.assign({}, execaOptions, {
                stdin: 'inherit',
                stdout: 'inherit'
              })
            )
          ).catch(err => {
            let message

            switch (err.exitCode) {
              case 255:
                message = chalk.red(
                  `Could not connect to host ${chalk.cyan(
                    host
                  )} as it was not recognized.`
                )
                break
              default:
                message = err.shortMessage
            }
            console.error(message)
          })
      }
    }
  } catch (err) {
    console.error(err)
  }
}

main()
