const chalk = require('chalk')
const { prompt } = require('enquirer')
const sshConfig = require('ssh-config')

const add = async options => {
  const userHost = options._.shift()
  const serverName = options._.shift()

  const userHostParams = userHost ? userHost.split('@') : [,]
  let user = userHostParams[0]
  let hostName = userHostParams[1] || user

  if (user === hostName) user = null

  if (!user || !options.identityFile || !hostName) {
    console.info(
      'SSH Menu will prompt you for answers but in the future you can use a single line:'
    )
    console.info(
      chalk.bold(
        'sshm -i <path to identity-file> <user>@<hostname> <connection name>'
      )
    )
    console.info(
      chalk.italic(
        'example: sshm -i ~/.ssh/github_rsa serveradmin@mydomain.com mydomain-prod\n'
      )
    )
  }

  const questions = []

  if (!serverName) {
    questions.push({
      type: 'input',
      name: 'name',
      message: 'What is the name for the ssh connection?'
    })
  }
  if (!options.identityFile) {
    questions.push({
      type: 'input',
      name: 'identityFile',
      message: 'What is the path to the identity file? (ex. ~/.ssh/myfile_rsa)'
    })
  }
  if (!user) {
    questions.push({
      type: 'input',
      name: 'user',
      message: 'What is the username you will use to connect?'
    })
  }
  if (!hostName) {
    questions.push({
      type: 'input',
      name: 'hostName',
      message: 'What is the server you will be connecting to (ip or name)?'
    })
  }

  console.info('Ask for details...')
  const server = await prompt(questions)

  if (server.name) {
    server.name = server.name.toLowerCase().replace(/\s/g, '-')
  }

  const newConfig = Object.assign(
    {},
    {
      user,
      hostName,
      port: options.port,
      name: serverName.replace(/\s/g, '-'),
      identityFile: options.identityFile
    },
    server
  )

  //const cfg = new sshConfig()
  return new sshConfig().append({
    Host: newConfig.name,
    HostName: newConfig.hostName,
    Port: newConfig.port,
    User: newConfig.user,
    IdentityFile: newConfig.identityFile
  })
}

module.exports = add
