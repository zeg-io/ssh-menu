const execa = require('execa')
const { Select } = require('enquirer')
const cmd = require('../command-parser')

const execaOptions = {
  stderr: 'inherit'
}

const run = async choices => {
  const promptSelect = new Select({
    name: 'template',
    message: 'Select a connection',
    choices: choices.concat({
      name: '> cancel <',
      message: 'Control+C to quit.'
    })
  })
  const choice = await promptSelect.run()

  if (!choice || choice === '> cancel <') return false

  const command = `ssh ${choice}`

  console.info('Executing:', command)

  return execa(
    ...cmd(
      command,
      Object.assign({}, execaOptions, {
        stdin: 'inherit',
        stdout: 'inherit'
      })
    )
  ).catch(err => {})
}

module.exports = run
