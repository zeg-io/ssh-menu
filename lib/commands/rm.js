const chalk = require('chalk')

const findFilter = hostToRemove => h =>
  h.param.toLowerCase() === 'host' && h.value === hostToRemove

const excludeFilter = hostToRemove => h =>
  h.param.toLowerCase() === 'host' && h.value !== hostToRemove

const rm = (hosts, hostToRemove) => {
  if (!hostToRemove) {
    return console.error(
      chalk.red('Missing host name. Ex. sshm rm my-connection')
    )
  }
  const existingHost = hosts.find(findFilter(hostToRemove))

  if (!existingHost) {
    return console.error(chalk.red('No host with that name'))
  }
  return hosts.filter(excludeFilter(hostToRemove))
}
module.exports = rm
