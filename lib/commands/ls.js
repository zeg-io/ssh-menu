const chalk = require('chalk')
const columnify = require('columnify')

const ls = hosts => {
  const columns = columnify(
    hosts.map(item => {
      let user, hostname, identityFile, port

      for (const configItem of item.config) {
        if (configItem.type !== 2) {
          switch (configItem.param.toLowerCase()) {
            case 'user':
              user = configItem.value
              break
            case 'hostname':
              hostname = configItem.value
              break
            case 'identityfile':
              identityFile = configItem.value
              break
            case 'port':
              port = configItem.value
              break
            default:
              console.info('Saw unknown param: ' + configItem.param)
          }
        }
      }

      return {
        'short name': chalk.bold.yellow(item.value),
        'identity File': chalk.bold.red(identityFile),
        connection: `${
          user ? `${chalk.cyan(user)}${chalk.white('@')}` : ''
        }${chalk.magenta(hostname)}`
      }
    })
  )
  console.info(columns)
}
module.exports = ls
