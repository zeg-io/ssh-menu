const chalk = require('chalk')

const createChoices = hosts =>
  hosts.map(item => {
    let user, hostname, identityFile, port

    for (const configItem of item.config) {
      if (configItem.type !== 2) {
        switch (configItem.param.toLowerCase()) {
          case 'user':
            user = configItem.value
            break
          case 'hostname':
            hostname = configItem.value
            break
          case 'identityfile':
            identityFile = configItem.value
            break
          case 'port':
            port = configItem.value
            break
          default:
            if (configItem.param.toLowerCase() === 'addkeystoagent') { break }
            console.info('Saw unknown param: ' + configItem.param)
        }
      }
    }
    // names with spaces show as arrays
    if (Array.isArray(item.value)) {
      item.value = item.value.join(' ')
    }
    return {
      name: item.value,
      message: `${chalk.bold.yellow(item.value.padEnd(25))}${
        user ? `${chalk.cyan(user)}${chalk.white('@')}` : ''
      }${chalk.magenta(hostname)}`,
      params: {
        user,
        hostname,
        port,
        identityFile
      }
    }
  }).filter(option => option.params.user)

module.exports = createChoices
