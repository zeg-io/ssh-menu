yargs = require('yargs')

module.exports = yargs
  .usage('Usage: sshm [options]')
  .command('add', 'Starts prompts to add a new menu item')
  .command(
    'add',
    '-i <~/.ssh/idfile_rsa> <user@hostserver.com> <menu-name> to add a new menu item'
  )
  .command('ls', 'Lists options with identity files')
  .command('rm', '<menu name> Removes an item from the menu options')
  .option('r', { alias: 'run', describe: 'Run an item, default' })
  .option('i', {
    alias: 'identity-file',
    describe: 'While adding, include an identity file.'
  })
  .option('p', {
    alias: 'port',
    describe: 'While adding, port to use.',
    default: 22
  })
  .option('u', {
    alias: 'user',
    describe: 'While adding, user to use.'
  })
  .options('H', {
    alias: 'host',
    describe: 'Host name (or IP) to use'
  })
  .option('v', {
    alias: 'version',
    describe: 'gets the version'
  }).argv
