# sshmenu

I wrote this because while working across multiple clients I was tired of constantly going back to see what the ssh config was, or worse, if I didn't add a server and identity to the config file, having to look up config info.  sshmenu lets you add and remove items from the ssh config file and pull up a menu, use arrow keys, and click on them!

## Installation
```bash
npm i -g sshmenu
```

## Usage

Pull up a list of existing hosts pick one, and hit enter to connect.

```bash
sshmenu
# or
sshm
```

To add a new host it is _almost_ the same as using ssh.

```bash
sshm add -i <identity file> <user>@<hostname or ip> <host alias>
# example
sshm add -i ~/.ssh/github_rsa admin@hostname.or.ip.com client-development
```

To remove an existing host

```bash
sshm rm <host alias>
# example
sshm rm client-development
```

